
# Table of Contents

1.  [Sign Locally Compiled Kernel Modules](#orge9d0f03)
    1.  [Supported GNU/Linux distributions](#orgc04c55e)


<a id="orge9d0f03"></a>

# Sign Locally Compiled Kernel Modules

As described in my [blog post](https://blog.lazy-evaluation.net/posts/linux/switching-to-secureboot.html), the idea of Secure Boot is to limit what
code can be executed in kernel space on a machine.  For the kernels
and modules of the distribution this works without a problem, but for
locally compiled modules, this poses a difficulty as the user does not
have those signing keys.

A straightforward way to fix this is to create and enroll a MOK
(Machine Owner Key) with a passphrase and use it to sign locally
compiled modules.  The user is prompted for the passphrase every time
and thus has full control over the allowed code.  This way, even
malicious software running as root cannot successfully sign locally
compiled software and thus undermine Secure Boot. The Secure Boot
implementation itself also ensures that no further keys can be
enrolled without the user noticing it.

This script can be used after every `apt upgrade` to sign any newly
(locally) compiled kernel modules with the MOK located in
`/var/lib/shim-signed/mok/MOK.priv`.  Refer to the [blog post](https://blog.lazy-evaluation.net/posts/linux/switching-to-secureboot.html) on how to
generate this. Only the dkms modules need to be handled as the
upstream modules are signed correctly.

Another way to fix the problem of locally generated modules is to
enroll an MOK without a passphrase (or a passphrase readable only by
root) and then allow `dkms` to automatically sign modules without user
interaction.  This approach opens a large loophole as code running
with root rights then will also be able to sign malicious code.  This
is the approach of newer Ubuntu systems.


<a id="orgc04c55e"></a>

## Supported GNU/Linux distributions

-   Debian Bookworm (12)
-   Debian Trixie (13)
-   Ubuntu 22.04

